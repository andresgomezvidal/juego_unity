# juego_unity
Juego en unity muy básico para la práctica final de la asignatura de computación gráfica.


Basado en https://unity3d.com/learn/tutorials/projects/space-shooter-tutorial

Típico juego cutre de asteroides y naves en el que te desplazas en una recta.
En vez de asteroides hay vacas.

La vaca del lateral no hace nada y está ahí solamente porque me gusta.

Controles:
    x: super ataque
    flechas o wasd: moverse
    click izquierdo o barra espaciadora: disparar
    no me acuerdo: cambiar dirección de disparo