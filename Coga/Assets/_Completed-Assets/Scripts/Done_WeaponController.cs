﻿using UnityEngine;
using System.Collections;

public class Done_WeaponController : MonoBehaviour
{
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	public float delay;

	private int cont=1;
	private float INCREMENTO_TIEMPO=50.0f;

	void Start ()
	{
		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Fire ()
	{
		for (float i = 0.0f; i < Time.time; i += INCREMENTO_TIEMPO) {
			Instantiate (shot, shotSpawn.position, Quaternion.Euler (0, 180 - i/INCREMENTO_TIEMPO*cont * 15, 0));
			Instantiate (shot, shotSpawn.position, Quaternion.Euler (0, 180 + i/INCREMENTO_TIEMPO*cont * 15, 0));
			Instantiate (shot, shotSpawn.position, Quaternion.Euler (0, 180, 0));
		}
		cont++;
		GetComponent<AudioSource>().Play();
	}
}
