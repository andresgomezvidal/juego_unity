﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girar : MonoBehaviour {
	private Done_PlayerController playerController;
	private float x;
	private float z;
	private bool stop;

	void Start ()
	{
		stop=false;
		getPlayerNow();
	}

	//si no al destruir el jugador saldrian un monton de errores benignos
	void getPlayerNow(){
		playerController = GameObject.FindObjectOfType(typeof(Done_PlayerController)) as Done_PlayerController;
		x=playerController.GetComponent<Rigidbody>().position.x-GetComponent<Rigidbody>().position.x;
		z=playerController.GetComponent<Rigidbody>().position.z-GetComponent<Rigidbody>().position.z;
	}
	void Update() {
		try{
			if(playerController!=null){
				//Vector3 vp=new Vector3(playerController.GetComponent<Rigidbody>().position.x, 0.0f, playerController.GetComponent<Rigidbody>().position.z);
				//Vector3 vp2=new Vector3(playerController.GetComponent<Rigidbody>().position.x-10, 0.0f, playerController.GetComponent<Rigidbody>().position.z+10);
				//transform.RotateAround(vp,vp2,1);
				//GetComponent<Rigidbody>().position=new Vector3(playerController.GetComponent<Rigidbody>().position.x+x+1*Mathf.Sin(Time.time), 0.0f, playerController.GetComponent<Rigidbody>().position.z+z-1*Mathf.Cos(Time.time));
				GetComponent<Rigidbody>().position=new Vector3(playerController.GetComponent<Rigidbody>().position.x+x, 0.0f, playerController.GetComponent<Rigidbody>().position.z+z);
			}
		}catch(NullReferenceException e){
			if(!stop){
				getPlayerNow();
			}
		}
	}
	public void Stop(){
		stop=true;
	}
	public void Resume(){
		stop=false;
	}
}
