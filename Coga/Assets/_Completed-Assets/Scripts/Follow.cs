﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {
	private Done_PlayerController playerController;
	private bool stop;

	void Start ()
	{
		stop=false;
		getPlayerNow();
	}

	//si no al destruir el jugador saldrian un monton de errores benignos
	void getPlayerNow(){
		playerController = GameObject.FindObjectOfType(typeof(Done_PlayerController)) as Done_PlayerController;
	}
	void Update() {
		try{
			if(playerController!=null){
				GetComponent<Rigidbody>().position=Vector3.MoveTowards(GetComponent<Rigidbody>().position, playerController.GetComponent<Rigidbody>().position, 0.09f);
			}
		}catch(NullReferenceException e){
			if(!stop){
				getPlayerNow();
			}
		}
	}
	public void Stop(){
		stop=true;
	}
	public void Resume(){
		stop=false;
	}
}
