﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBoss : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().velocity = transform.forward * -5;
	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<Rigidbody>().position.z<13)
			GetComponent<Rigidbody>().velocity = transform.forward * 0;
	}
}
