﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Done_GameController : MonoBehaviour
{
	public GameObject[] hazards;
	public GameObject boss_object;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	
	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	
	private bool gameOver;
	private bool restart;
	public int score;
	private static AudioSource audio;
	private static float atime=0.0f;
	private static bool boss_call=false;
	private static bool boss_alive=false;
	private static float boss_time=0;
	
	private static Girar girar;
	private static Follow follow;

	void Start ()
	{
		girar=new Girar();
		girar.Resume();
		follow=new Follow();
		follow.Resume();
		audio=GetComponent<AudioSource>();
		audio.time=atime;
		audio.Play();
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
	}
	
	void Update ()
	{
		if((Time.time-boss_time)>30){
			boss_time=Time.time;
			boss_call=true;
		}
		if (restart)
		{
			BossDeath();
			girar.Stop();
			follow.Stop();
			if (Input.GetKeyDown (KeyCode.R))
			{
				atime=audio.time;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}
	}
	
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			if(boss_call){
				if(!boss_alive){
					Vector3 spawnPosition = new Vector3 (0.0f, 0.0f, spawnValues.z);
					Quaternion spawnRotation = Quaternion.identity;
					Instantiate (boss_object, spawnPosition, spawnRotation);
					boss_alive=true;
				}
				yield return new WaitForSeconds (10*waveWait);
			}else{
				for (int i = 0; i < hazardCount; i++)
				{
					GameObject hazard = hazards [Random.Range (0, hazards.Length)];
					Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
					Quaternion spawnRotation = Quaternion.identity;
					Instantiate (hazard, spawnPosition, spawnRotation);
					yield return new WaitForSeconds (spawnWait);
				}
				yield return new WaitForSeconds (waveWait);
			}
			
			if (gameOver)
			{
				restartText.text = "R para Reiniciar";
				restart = true;
				break;
			}
		}
	}
	
	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}
	
	void UpdateScore ()
	{
		scoreText.text = "Nota: " + score;
	}
	
	public void GameOver ()
	{
		gameOverText.text = "Suspenso!";
		gameOver = true;
	}
	public void BossDeath(){
		boss_call=false;
		boss_alive=false;
		boss_time=Time.time;
	}
}
