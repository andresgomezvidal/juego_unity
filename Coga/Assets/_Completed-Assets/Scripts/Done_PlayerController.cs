﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Done_Boundary 
{
	public float xMin, xMax, zMin, zMax;
}

public class Done_PlayerController : MonoBehaviour
{
	public float speed;
	public float tilt;
	public Done_Boundary boundary;

	public GameObject shot;
	public GameObject shot2;
	public GameObject shield;
	public Transform shotSpawn;
	public Transform shotSpawn2;
	private Transform shotShield;
	public float fireRate;
	public GUIText SuperAtaque;
	public GUIText SobreCarga;
	public GUIText Tiempo;
	
	private float SobreCargaF=0.0f;
	private float nextFire;
	private float nextSuperFire;
	private float ENFRIAMIENTO=.15f;
	private float Giro=0.0f;
	private float CARGA_DISPARO=2;
	private float SuperAtaque_V2 = 0;
	private static float SuperAtaque_Max=3;
	private static int disparos=2;
	private float shield_position=0.0f;

	void Start(){
		shotShield=shotSpawn2;
	}

	void Update ()
	{
		//giro del disparo
		if (Input.GetKey (KeyCode.Z)) {
			Giro--;
		}else if (Input.GetKey (KeyCode.V)) {
			Giro++;
		}

		if (SuperAtaque_V2 > 1) {
			if(SuperAtaque_V2%3==0){
				shotSpawn2.rotation = Quaternion.Euler (0, SuperAtaque_V2*5, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
				shotSpawn2.rotation = Quaternion.Euler (0, -SuperAtaque_V2*5, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			}
			else{
				shotSpawn2.rotation = Quaternion.Euler (0, SuperAtaque_V2*1.3f, 0);
				Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);
				shotSpawn2.rotation = Quaternion.Euler (0, -SuperAtaque_V2*1.3f, 0);
				Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);

				shotSpawn2.rotation = Quaternion.Euler (0, SuperAtaque_V2*1.6f, 0);
				Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);
				shotSpawn2.rotation = Quaternion.Euler (0, -SuperAtaque_V2*1.6f, 0);
				Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);

				shotSpawn2.rotation = Quaternion.Euler (0, SuperAtaque_V2, 0);
				Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);
				shotSpawn2.rotation = Quaternion.Euler (0, -SuperAtaque_V2, 0);
				Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);
			}
			SuperAtaque_V2--;
		}
		else if(SuperAtaque_V2==1){
			shotSpawn2.rotation = Quaternion.Euler (0, 12.5f, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			shotSpawn2.rotation = Quaternion.Euler (0, -12.5f, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			shotSpawn2.rotation = Quaternion.Euler (0, 33.5f, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			shotSpawn2.rotation = Quaternion.Euler (0, -33.5f, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);

			shotSpawn2.rotation = Quaternion.Euler (0, 185, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			shotSpawn2.rotation = Quaternion.Euler (0, -185, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			shotSpawn2.rotation = Quaternion.Euler (0, 225, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			shotSpawn2.rotation = Quaternion.Euler (0, -225, 0);
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation);
			SuperAtaque_V2--;
		}

		if (SobreCargaF < 100) {
			if ((Input.GetButton ("Fire1") || Input.GetKey (KeyCode.Space)) && Time.time > nextFire) {
				nextFire = Time.time + fireRate;
				Instantiate (shot, shotSpawn.position, Quaternion.Euler (0, Giro, 0));
				for(float i=1.0f;i<=disparos-2;i+=5) {
					Instantiate (shot2, shotSpawn.position, Quaternion.Euler (0, -1+Giro-i*4, 0));
					Instantiate (shot2, shotSpawn.position, Quaternion.Euler (0, 1+Giro+i*4, 0));
				}
				for(float i=0.0f;i<=disparos-2;i+=2) {
					Instantiate (shot, shotSpawn.position, Quaternion.Euler (0, Giro+i*2.5f, 0));
					Instantiate (shot, shotSpawn.position, Quaternion.Euler (0, Giro-i*2.5f, 0));
				}
				if(disparos*CARGA_DISPARO>20)
					SobreCargaF+=20;
				else
					SobreCargaF+=disparos*CARGA_DISPARO;
				GetComponent<AudioSource> ().Play ();
			} else {
				SobreCargaF-=ENFRIAMIENTO;
			}
			if (Time.time > nextSuperFire) {
				SuperAtaque.text = "LISTO";
				SuperAtaque.color=UnityEngine.Color.green;
				if (Input.GetKey (KeyCode.S)||Input.GetKey (KeyCode.X)) {
					nextFire = Time.time + 2 * fireRate;
					nextSuperFire = Time.time + 5;
					for (int i = 0; i < 360; i += 10) {
						shotSpawn2.rotation = Quaternion.Euler (0, i, 0);
						Instantiate (shot, shotSpawn2.position, shotSpawn2.rotation);
						SuperAtaque.text = "Recargando...";
						SuperAtaque.color=UnityEngine.Color.red;
						SobreCargaF++;
					}
					SuperAtaque_V2 = SuperAtaque_Max;
					GetComponent<AudioSource> ().Play ();
				}
			}

		} else {
			SobreCargaF-=ENFRIAMIENTO;
		}

		if (SobreCargaF < 0)
			SobreCargaF = 0;
		if(SobreCargaF>70)
			SobreCarga.color = UnityEngine.Color.red;
		else if(SobreCargaF>25)
			SobreCarga.color = UnityEngine.Color.yellow;
		else
			SobreCarga.color = UnityEngine.Color.white;
		SobreCarga.text = Mathf.RoundToInt(SobreCargaF).ToString()+"%";
		Tiempo.text = Mathf.RoundToInt((float)Time.time).ToString()+" S";
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		GetComponent<Rigidbody>().velocity = movement * speed;
		
		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp (GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
		);
		
		GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
	}

	public void Upgrade ()
	{
		disparos++;
		SuperAtaque_Max++;

		//escudo
		float distancia=2;
		float pir3=360/(2*3.1416f);
		//if(shield_position<360){
		if(shield_position<pir3){
			shotShield.rotation = Quaternion.Euler (0, shield_position, 0);
			shotShield.position = new Vector3 (GetComponent<Rigidbody>().position.x+distancia*Mathf.Sin(shield_position), 0.0f,GetComponent<Rigidbody>().position.z- distancia*Mathf.Cos(shield_position));
			Instantiate (shield, shotShield.position, shotShield.rotation);
			//shield_position+=180/(2*3.1416f);
			//shield_position+=36;
			shield_position+=pir3/8.0f;
		}else
			shield_position-=(pir3/16.0f+pir3);
		//for(float i=0.0f;i<360;i+=180/(2*3.1416f)) {
			//shotShield.rotation = Quaternion.Euler (0, i, 0);
			//shotShield.position = new Vector3 (GetComponent<Rigidbody>().position.x-distancia*Mathf.Cos(i), 0.0f,GetComponent<Rigidbody>().position.z+ distancia*Mathf.Sin(i));
			//Instantiate (shield, shotShield.position, shotShield.rotation);
		//}
	}
}
