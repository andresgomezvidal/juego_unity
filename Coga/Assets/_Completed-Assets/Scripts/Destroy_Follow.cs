﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_Follow : MonoBehaviour {
	public GameObject playerExplosion;
	private Done_GameController gameController;

	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <Done_GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player")
		{
			Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver();
			Destroy (other.gameObject);
			Destroy (gameObject);
		}else
			return;
	}
}
