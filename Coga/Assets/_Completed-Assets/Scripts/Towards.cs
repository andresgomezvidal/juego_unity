﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Towards : MonoBehaviour {
	private Done_PlayerController playerController;
	private float speed=1.0f;

	void Start ()
	{
		playerController = GameObject.FindObjectOfType(typeof(Done_PlayerController)) as Done_PlayerController;
		try{
			if(playerController!=null){
				GetComponent<Rigidbody>().position=Vector3.MoveTowards(GetComponent<Rigidbody>().position, playerController.GetComponent<Rigidbody>().position, speed);
				GetComponent<Rigidbody>().velocity = transform.forward * speed;
			}
		}catch(NullReferenceException e){
			print(e);
		}
	}
}
