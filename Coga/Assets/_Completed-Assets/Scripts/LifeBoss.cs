﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBoss : MonoBehaviour {
	public GameObject explosion;
	private Done_GameController gameController;
	private float life=100;

	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <Done_GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}
	
	// Update is called once per frame
	//void Update () {
	//}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag != "Boundary" && other.tag != "Enemy"){
			life-=2.5f;
			Instantiate(explosion, other.transform.position, other.transform.rotation);
			if(life<=0.0f){
				gameController.BossDeath();
				Destroy (other.gameObject);
				Destroy (gameObject);
			}
		}
	}
}
