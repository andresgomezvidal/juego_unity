﻿using UnityEngine;
using System.Collections;

public class Done_DestroyByBoundary : MonoBehaviour
{
	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Boss"||other.tag == "Shield")
		{
			return;
		}
		Destroy(other.gameObject);
	}
}
