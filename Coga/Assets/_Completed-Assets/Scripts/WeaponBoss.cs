﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBoss : MonoBehaviour {
	public GameObject shot;
	public GameObject shot2;
	public GameObject shot_follow;
	public Transform shotSpawn;
	public Transform shotSpawn2;
	public Transform shotSpawn3;
	public float fireRate;
	public float delay;

	private int cont;
	private int cont2;
	private float inicio;

	void Start ()
	{
		cont=0;
		cont2=10;
		inicio=Time.time;									//para darle un retraso antes de empezar a disparar
		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Fire ()
	{
		if(Time.time-inicio>1){
			//ataque frontal tocho
			if(cont==0){
				for( float t=0.0f; t<5; t++)
				{
					Instantiate (shot, shotSpawn.position, Quaternion.Euler (0,   180 + t*1.2f, 0));
					Instantiate (shot, shotSpawn.position, Quaternion.Euler (0,   180 - t*1.2f, 0));
				}
				cont++;
			}
			else if(cont<=10){
				if(cont%2==0){
					if(cont%3==0){
						Instantiate (shot2, shotSpawn2.position, Quaternion.Euler (0, 180 - cont*3.5f, 0));
						Instantiate (shot2, shotSpawn3.position, Quaternion.Euler (0, 180 + cont*3.5f, 0));
						Instantiate (shot, shotSpawn.position, Quaternion.Euler   (0, 180 - cont*3.5f, 0));
						Instantiate (shot, shotSpawn.position, Quaternion.Euler   (0, 180 + cont*3.5f, 0));
					}
					else{
						Instantiate (shot2, shotSpawn3.position, Quaternion.Euler (0, 180 - cont*3.5f, 0));
						Instantiate (shot2, shotSpawn2.position, Quaternion.Euler (0, 180 + cont*3.5f, 0));
						Instantiate (shot,  shotSpawn.position, Quaternion.Euler  (0, 180 - cont*2.5f, 0));
						Instantiate (shot,  shotSpawn.position, Quaternion.Euler  (0, 180 + cont*2.5f, 0));
					}
				}else{
					if(cont%3==0){
						Instantiate (shot2, shotSpawn2.position, Quaternion.Euler (0, 180 - 2*3.5f, 0));
						Instantiate (shot2, shotSpawn3.position, Quaternion.Euler (0, 180 + 2*3.5f, 0));
						Instantiate (shot, shotSpawn.position, Quaternion.Euler   (0, 180 - 2*3.5f, 0));
						Instantiate (shot, shotSpawn.position, Quaternion.Euler   (0, 180 + 2*3.5f, 0));
					}
					else{
						Instantiate (shot2, shotSpawn3.position, Quaternion.Euler (0, 180 - 4*3.5f, 0));
						Instantiate (shot2, shotSpawn2.position, Quaternion.Euler (0, 180 + 4*3.5f, 0));
						Instantiate (shot,  shotSpawn.position, Quaternion.Euler  (0, 180 - 4*2.5f, 0));
						Instantiate (shot,  shotSpawn.position, Quaternion.Euler  (0, 180 + 4*2.5f, 0));
					}
				}
				cont++;
			}
			else if(cont<=20){
				//diferencia de 7 para que descanse un momento y le dea tiempo a los proyectiles a desaparecer
				if(cont<=13){
					//dispara a la posicion del jugador, actualizandose durante el tiempo de vida
					Instantiate (shot_follow,    shotSpawn.position, Quaternion.Euler (0,  180, 0));
				}
				cont++;
			}
			else{
				if(cont2>15){
					cont=0;
					cont2=10;
				}
				else{
					Instantiate (shot2, shotSpawn2.position, Quaternion.Euler (0, 180 - cont2*5.5f, 0));
					Instantiate (shot2, shotSpawn3.position, Quaternion.Euler (0, 180 + cont2*5.5f, 0));
					Instantiate (shot, shotSpawn.position, Quaternion.Euler   (0, 180 - cont2*4.5f, 0));
					Instantiate (shot, shotSpawn.position, Quaternion.Euler   (0, 180 + cont2*4.5f, 0));
					Instantiate (shot,  shotSpawn.position, Quaternion.Euler  (0, 180 - cont2*1.5f, 0));
					Instantiate (shot,  shotSpawn.position, Quaternion.Euler  (0, 180 + cont2*1.5f, 0));
					cont2++;
				}
			}
			GetComponent<AudioSource>().Play();
		}
	}
}


