using UnityEngine;
using System.Collections;

public class Gift_weapon : MonoBehaviour
{
	public GameObject explosion;
	private Done_PlayerController playerController;

	void Start ()
	{
		playerController = GameObject.FindObjectOfType(typeof(Done_PlayerController)) as Done_PlayerController;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player")
		{
			if (explosion != null)
			{
				Instantiate(explosion, transform.position, transform.rotation);
			}
			playerController.Upgrade();
			Destroy (gameObject);
		}
	}
}


